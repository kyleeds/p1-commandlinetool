//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>

//function definition
void factorialFunction(int intNum);

int main (int argc, const char* argv[])
{

    int intNum;
    
    // insert code here...
    
    //Retrieving the intNum value from the user.
    std::cin >> intNum;
    
    //Use the function
    factorialFunction(intNum);
    
    return 0;
}

//function declaration
void factorialFunction(int intNum){
    std::cout << "input value " << intNum << std::endl;
    int factorialNum;
        for (factorialNum = 1;intNum>1;intNum--){
            factorialNum *= intNum;
    }
    std::cout << "factorial value " << factorialNum << std::endl;
}